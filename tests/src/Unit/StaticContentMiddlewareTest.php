<?php

namespace Drupal\Tests\static_routes\Unit;

use Drupal\Core\Site\Settings;
use Drupal\static_routes\StackMiddleware\StaticContentNegotiator;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * @coversDefaultClass \Drupal\static_routes\StackMiddleware\StaticContentNegotiator
 * @group static_routes
 */
class StaticContentMiddlewareTest extends UnitTestCase {

  /**
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $mockHttpKernel;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $this->mockHttpKernel = $this->getMock(HttpKernelInterface::class);
  }

  /**
   * @covers ::matchPath
   */
  public function testMatchPath() {
    $blog_paths = "/blog\n/blog/*";
    // Make sure the home page misses blog posts.
    self::assertFalse(StaticContentNegotiator::matchPath('/', $blog_paths));
    // Make sure blog posts are matched.
    self::assertTrue(StaticContentNegotiator::matchPath('/blog', $blog_paths));
    self::assertTrue(StaticContentNegotiator::matchPath('/blog/', $blog_paths));
    self::assertTrue(StaticContentNegotiator::matchPath('/blog/foo', $blog_paths));
    self::assertTrue(StaticContentNegotiator::matchPath('/blog/foo/bar/baz', $blog_paths));
    self::assertTrue(StaticContentNegotiator::matchPath('/blog/foo/bar/baz/quz', $blog_paths));
  }

  /**
   * @covers ::handle
   */
  public function testStaticContentMiddleware() {
    $settings = new Settings([]);
    $this->assertEquals(0, $settings->get('static_routes'));

    $middleware = new StaticContentNegotiator($this->mockHttpKernel, $settings);

    self::assertNull($middleware->handle(Request::create('/'), $settings));

    $static_routes = ["/blog\n/blog/*" => __DIR__ . '/../../assets/index.html'];
    $settings = new Settings(['static_routes' => $static_routes]);
    $this->assertEquals($static_routes, $settings->get('static_routes'));

    $middleware = new StaticContentNegotiator($this->mockHttpKernel, $settings);
    self::assertNull($middleware->handle(Request::create('/'), $settings));
    self::assertNotNull($middleware->handle(Request::create('/blog'), $settings));
    self::assertNotNull($middleware->handle(Request::create('/blog/foo'), $settings));

    /** @var \Symfony\Component\HttpFoundation\BinaryFileResponse $response */
    $response = $middleware->handle(Request::create('/blog/foo/bar'), $settings);

    self::assertSame('text/html', $response->getFile()->getMimeType());
    self::assertSame(200, $response->getStatusCode());
    self::assertNotSame('no-cache', $response->headers->get('cache-control'));
  }

}

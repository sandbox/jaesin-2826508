<?php

namespace Drupal\static_routes\StackMiddleware;

use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class StaticContentNegotiator implements HttpKernelInterface {

  /**
   * The decorated kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $http_kernel;

  /**
   * The site settings.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;


  /**
   * Constructs a ReverseProxyMiddleware object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   * @param \Drupal\Core\Site\Settings $settings
   *   The site settings.
   */
  public function __construct(HttpKernelInterface $http_kernel, Settings $settings) {
    $this->http_kernel = $http_kernel;
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE) {
    // Get the static routes from settings.
    $static_routes = $this->settings->get('static_routes');
    // Search and validate static route match.
    if (!empty($static_routes) && ($static_file_path = static::searchStaticRoutes($static_routes, $request->getPathInfo()))) {
      // Return a new response the contents of the static asset.
      return BinaryFileResponse::create($static_file_path);
    }
    // Let the Drupal stack do it's thing.
    return $this->http_kernel->handle($request, $type, $catch);
  }

  public static function searchStaticRoutes($static_routes, $path_info) {
    // Use static caching.
    static $match = FALSE;

    // Check to se if we still need to do the actual matching.
    if ($match === FALSE) {
      if (!empty($static_routes)) {
        // Search for a matching pattern and serve the static resource.
        foreach ($static_routes as $pattern => $resource) {
          if (static::matchPath($path_info, $pattern) && file_exists($resource)) {
            // Return the fill path of the resource relative to Drupal root.
            $match = $resource;
            return $match;
          }
        }
      }
    }

    return $match;
  }

  /**
   * Bypass any further drupal bootstrapping and return a binary file if there
   * is a static route match. This is a performance oriented middleware bypass
   * method so we might not care about playing nice. If you aren't worried about
   * performance for some odd reason you can just use the middleware and the
   * `static_routes` settings in `settings.php` to serve up static content.
   *
   * @param $static_routes
   */
  public static function fastPass($static_routes) {
    // Static method so get the request object from globals.
    global $request;

    // Check for a static route match.
    if ($request && ($static_file_path = static::searchStaticRoutes($static_routes, $request->getPathInfo()))) {
      // Send a binary file response.
      BinaryFileResponse::create($static_file_path)->prepare($request)->send();
      // Using `global $kernel; $kernel->terminate($request, $response)` kills performance so.
      die(0);
    }
  }

  /**
   * Path matcher from the path.matcher service. It is duplicated here because
   * we sometimes need to match paths before the container has been initialized.
   *
   * This version does not match `<front>` like the services matcher does
   * because that would require loading config.
   *
   * @param string $path
   *   The path to match.
   * @param string $patterns
   *   A set of patterns separated by a newline.
   *
   * @return bool
   *   TRUE if the path matches a pattern, FALSE otherwise.
   */
  public static function matchPath($path, $patterns) {
    static $regex_patterns = [];
    if (!isset($regex_patterns[$patterns])) {
      // Convert path settings to a regular expression.
      $to_replace = array(
        // Replace newlines with a logical 'or'.
        '/(\r\n?|\n)/',
        // Quote asterisks.
        '/\\\\\*/',
      );
      $replacements = array(
        '|',
        '.*',
      );
      $patterns_quoted = preg_quote($patterns, '/');
      $regex_patterns[$patterns] = '/^(' . preg_replace($to_replace, $replacements, $patterns_quoted) . ')$/';
    }

    return (bool) preg_match($regex_patterns[$patterns], $path);
  }

}

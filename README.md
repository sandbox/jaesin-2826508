## Static routes for Drupal

### Motivation

It's true that you could add some static HTML files to your Drupal site if you
wanted to but URL paths these days aren't always that simple. In the spirit of
maintaining nice URLs this module aims to provide a mechanism serve static files
from arbitrary paths that would normally be handled by the routing system.

### Usage

Static routes are defined in `settings.php` as an array of static assets keyed
by a route match pattern. The pattern syntax is the same as what's used by the
`path.matcher` service except `<front>` is not supported because it requires
loading config which adds additional overhead to the delivery process.

Example:

```php
// Define route patterns for static assets.
$settings['static_routes'] = [
  "/blog\n/blog/*" => DRUPAL_ROOT . '/assets/static/blog/index.html',
];
```
### Performance

The default implementation uses a HTTP middleware. The middleware will search
for static route matches and deliver static assets via a `BinaryFileResponse`.
By the time the middleware gets called, Drupal has already done quite a bit of
work. For this reason, the best throughput we were able to achieve is ~600
requests / second with an average latency of ~170 milliseconds. It's not bad but
compared to serving a static html file via nginx (~32K Req/sew) it could use
some improvement. For reference a Drupal node page tested at ~190 requests/s
with a latency of ~520 milliseconds.

So a faster method was created to help with throughput and latency. Using
`static_routes_fast_pass()` is optional but can be used to help performance. It
was designed to be used directly in `settings.php` but it could also be used in
an altered `index.php` file.

As implemented in `settings.php`:

```php
// Define route patterns for static assets.
$settings['static_routes'] = [
  "/blog\n/blog/*" => DRUPAL_ROOT . '/assets/static/blog/index.html',
];

// Use the static route fast pass to increase performance.
if(file_exists(DRUPAL_ROOT . '/modules/contrib/static_routes/static_routes.module')) {
  include_once DRUPAL_ROOT . '/modules/contrib/static_routes/static_routes.module';
  static_routes_fast_pass($settings['static_routes']);
}
```

This function will die early once it has written the file to the output buffer
so it is able to achieve ~4000 requests per second with an average latency of
~30 milliseconds.

Testing was done on a local nginx setup with php 7.1 via php-fpm. Wrk was used
with 20 threads: `wrk -t20 -c400 -d10s http://foo.com/blog`.

This is only really useful for static assets with public URLs If you are
embedding assets in a page or referencing JS/CSS, it is better to link to them
via their static paths to avoid the overhead of bootstrapping drupal at all.
